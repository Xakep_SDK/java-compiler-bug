package dk.xakeps.compilerbug;

import com.github.mizosoft.methanol.TypeRef;

import java.util.List;

public class Main {
    public static void main(String[] args) {
    }

    public void crash() {
        // crashes
        List<String> response = wrap(ofObject(new TypeRef<>() {}));

        // does not crash
        List<String> response2 = wrap(ofObject(new TypeRef<List<String>>() {}));

        // does not crash
        List<String> response3 = ofObject(new TypeRef<>() {});
    }

    public static <T> T ofObject(TypeRef<T> type) {
        throw new RuntimeException();
    }

    public <T> T wrap(T arg) {
        throw new RuntimeException();
    }
}
